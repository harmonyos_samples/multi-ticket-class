## Responsive Layouts for Stock Trading Apps Across Devices

### Overview

This sample shows how to use the grid layout and **List** component to create responsive content layouts for stock trading apps across devices, such as smartphones, foldable screens, and tablets.

### Preview

Below are the preview effects.

**Tab adaptive centering**

| Smartphone                             | Foldable Screen (Unfolded)             | Tablet、2in1                            |
|----------------------------------------|----------------------------------------|----------------------------------------|
| ![](screenshots/Devices/image1.en.png) | ![](screenshots/Devices/image2.en.png) | ![](screenshots/Devices/image3.en.png) |

**Adaptive table extension**

| Smartphone                             | Foldable Screen (Unfolded)             | Tablet、2in1                                 |
|----------------------------------------|----------------------------------------|----------------------------------------|
| ![](screenshots/Devices/image4.en.png) | ![](screenshots/Devices/image5.en.png) | ![](screenshots/Devices/image6.en.png) |

**Adaptive centering of two tabs**

| Smartphone                             | Foldable Screen (Unfolded)             | Tablet、2in1                                 |
|----------------------------------------|----------------------------------------|----------------------------------------|
| ![](screenshots/Devices/image7.en.png) | ![](screenshots/Devices/image8.en.png) | ![](screenshots/Devices/image9.en.png) |

### How to Use

You can view the page effect in the Previewer of DevEco Studio or on the device.

### Project Directory

```
├──entry/src/main/ets                             // Code
│  ├──constants                                  
│  │  ├──CommonConstants.ets                       // Common constants
│  │  └──ListDataConstants.ets                     // List data constants
│  ├──entryability  
│  │  └──EntryAbility.ets 
│  ├──pages  
│  │  ├──AdaptiveTabCenteringIndex.ets             // Tab layout that adapts and centers itself based on the screen size.
│  │  ├──DoubleTabAdaptiveCenteringIndex.ets       // Tab layout that allows two sets of tabs to be centered and adapt to different screen sizes
│  │  ├──Index.ets                                 // Home page                                 
│  │  └──TableAdaptiveExtensionIndex.ets           // Implementation of tables that extend and adapt based on the screen size.
│  └──utils
│     ├──BreakpointType.ets                        // Breakpoint types 
│     └──Logger.ets                                // Logging utility        
└──entry/src/main/resources                        // Resources of the app

```

### How to Implement

* Use the grid layout to listen for breakpoint changes. Use different spacing between subcomponents of different breakpoint list components. Set **justifyContent** of the Flex layout to **FlexAlign.Center** to implement center alignment and adaptive stretching.
* Use the **Blank** component to implement adaptive stretching of spaces in the middle.

### Required Permissions

N/A

### Dependencies

N/A

### Constraints

1. The sample app is supported on Huawei phones, tablets, and 2in1 running the standard system.

2. The HarmonyOS version must be HarmonyOS 5.0.0 Release or later.

3. The DevEco Studio version must be DevEco Studio 5.0.0 Release or later.

4. The HarmonyOS SDK version must be HarmonyOS 5.0.0 Release SDK or later.
