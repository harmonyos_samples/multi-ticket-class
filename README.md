## 一次开发，多端部署-股票应用

### 介绍

本示例主要使用栅格布局和List组件相结合的方式，实现了股票类差异化的多场景响应式变化效果。

### 效果预览

本示例在预览器中的效果：

**tab自适应居中**:

| 手机                                 | 折叠屏（展开态）                       | 平板、2in1                             |
|-------------------------------------|-------------------------------------|-------------------------------------|
| ![](screenshots/Devices/image1.png) | ![](screenshots/Devices/image2.png) | ![](screenshots/Devices/image3.png) |

**表格自适应延伸**:

| 手机                                 | 折叠屏（展开态）                       | 平板、2in1                                |
|-------------------------------------|-------------------------------------|-------------------------------------|
| ![](screenshots/Devices/image4.png) | ![](screenshots/Devices/image5.png) | ![](screenshots/Devices/image6.png) |

**双tab自适应居中**:

| 手机                                 | 折叠屏（展开态）                       | 平板、2in1                                 |
|-------------------------------------|-------------------------------------|-------------------------------------|
| ![](screenshots/Devices/image7.png) | ![](screenshots/Devices/image8.png) | ![](screenshots/Devices/image9.png) |

使用说明：

1.可以在预览器中查看页面效果，也可在对应设备上查看页面效果。

### 工程目录

```
├──entry/src/main/ets                              // 代码区
│  ├──constants                                  
│  │  ├──CommonConstants.ets                       // 常用常量
│  │  └──ListDataConstants.ets                     // 列表数据常量
│  ├──entryability  
│  │  └──EntryAbility.ets 
│  ├──pages  
│  │  ├──AdaptiveTabCenteringIndex.ets             // tab自适应居中页
│  │  ├──DoubleTabAdaptiveCenteringIndex.ets       // 双tab自适应居中页 
│  │  ├──Index.ets                                 // 首页                                  
│  │  └──TableAdaptiveExtensionIndex.ets           // 表格自适应延伸页
│  └──utils
│     ├──BreakpointType.ets                        // 断点类型 
│     └──Logger.ets                                // 日志        
└──entry/src/main/resources                        // 应用资源目录

```

### 具体实现

* 使用栅格布局监听断点变化，不同断点List组件的子组件间隔不同，同时设置Flex布局的justifyContent属性为FlexAlign.Center，实现居中对齐自适应拉伸。
* 使用Blank组件实现中间空格自适应拉伸。

### 相关权限

不涉及。

### 依赖

不涉及。

### 约束与限制

1. 本示例仅支持标准系统上运行，支持设备：华为手机、平板、2in1。

2. HarmonyOS系统：HarmonyOS 5.0.0 Release及以上。

3. DevEco Studio版本：DevEco Studio 5.0.0 Release及以上。

4. HarmonyOS SDK版本：HarmonyOS 5.0.0 Release SDK及以上。
